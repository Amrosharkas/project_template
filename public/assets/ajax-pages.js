$(document).on('click', '.ajax-link', function (e) {
    e.preventDefault();
    ajaxPage($(this).attr('href'));
    return false;
});
window.onpopstate = function (event) {
    $('#ajax-container').load(location.pathname, function () {
        $(document).trigger('ajax-page-loaded');
    });
}
function ajaxPage(url) {
    history.pushState('', 'New URL: ' + url, url);
    $('#ajax-container').load(url, function () {
        $(document).trigger('ajax-page-loaded');
    });
}
function ajaxReload() {
    $('#ajax-container').load(location.pathname, function () {
        $(document).trigger('ajax-page-loaded');
    });
}