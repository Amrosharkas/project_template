// prepare the form when the DOM is ready 
$(document).on('ready pjax:success', function () {
    $("input,select").change(function () {
        $(this).removeClass("errorValidation");
        $(this).next(".error_message").hide();
    });
    $(".select2").select2({
      placeholder: "",
      allowClear: true
    });
    var options = {

        beforeSubmit: beforeSubmit, // pre-submit callback 
        success: success, // post-submit callback 
        error: error


    };
    // bind form using 'ajaxForm' 
    $('.ajax_form').ajaxForm(options);
});

// pre-submit callback 
function beforeSubmit(formData, jqForm, options) {
    appBlockUI();
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData);


    var formElement = jqForm[0];
    var pass = 1;
    $(".error_message").hide();
    for (var i = 0; i < formData.length; i++) {
        var elementName = formData[i].name.replace("[]", ""); 
        if (formData[i].type == "select-one") {
            $formElement = $('select[name=' + elementName + ']');
        } else {

            $formElement = $('input[name=' + elementName + ']');
        }

        $formElement.removeClass("errorValidation");

        validationRules = $formElement.attr('data-validation');
        validationName = $formElement.attr('data-name');
        
        if (typeof validationRules !== 'undefined') {
            if (validationRules.includes("match")) {

                var split = validationRules.split(".");
                var field = split[1];
                var fieldValue = $('input[name=' + elementName + ']').val();
                if (formData[i].value !== fieldValue) {
                    appUnBlockUI();
                    //var $toast = toastr["error"]("Error", validationName+" is required");  
                    $formElement.addClass("errorValidation");
                    $formElement.next(".error_message").show().html(field + "s don't match");
                    pass = 0;
                }
            }
            if (!validateEmail(formData[i].value) && validationRules.includes("email")) {
                appUnBlockUI();
                //var $toast = toastr["error"]("Error", " Invalid Email");
                $formElement.addClass("errorValidation");
                $formElement.next(".error_message").show().html("Invalid Email");
                pass = 0;
            }
            if (isNaN(formData[i].value) && validationRules.includes("number")) {
                appUnBlockUI();
                //var $toast = toastr["error"]("Error", validationName+" must be a number");
                $formElement.addClass("errorValidation");
                $formElement.next(".error_message").show().html(validationName + " must be a number");
                pass = 0;
            }
            if (formData[i].value == "" && validationRules.includes("required")) {
                appUnBlockUI();
                //var $toast = toastr["error"]("Error", validationName+" is required");  
                $formElement.addClass("errorValidation");
                $formElement.next(".error_message").show().html(validationName + " is required");
                pass = 0;
            }
        }
    }
    if (pass == 1) {
        return true;
    } else {
        return false;
    }
}

// post-submit callback 
function success(responseText, statusText, xhr, $form) {
    appUnBlockUI();
    var response = responseText;
    if (response.status == "error") {
        var $toast = toastr["error"](response.msg, "Sorry");
    } else {
        var $toast = toastr["success"](response.msg, "Success");
        if (response.page == "none") {
            $(".resetForm").click();
        } else {
            pjaxPage(response.page);
        }
    }
}

function error(responseText, statusText, xhr, $form) {
    appUnBlockUI();
    var $toast = toastr["error"](responseText, statusText);

}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
