<?php
$i = 'tasks';
if(isset($status)){
$j = $status;
}else{
	$j = "";
}
?>
@extends('admin.master')
@section('plugins_css')
<link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/dragula/dragula.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.css')}}"/>
<style>
.update_table td{
	padding: 0 !important;
	text-align:left;
}
.update_table .btn{
	margin:0 !important;
	padding:0 !important;
}
.timepickerDiv{
	width:400px;
}
</style>
@endSection

@section('plugins_js')
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jsvalidation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.js"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/dragula/dragula.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.js')}}"></script>
@endSection

@section('page_js')
<script type="text/javascript" src="{{asset('assets/scripts.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/task.js')}}"></script>
@endSection

@section('add_inits')

@stop

@section('title')
Tasks
@stop

@section('page_title')
Tasks
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

