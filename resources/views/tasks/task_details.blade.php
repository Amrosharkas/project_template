<h3>{{$task->name}}</h3>
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr>
    <th width="16%" align="left" valign="middle" scope="row">Software</th>
    <td width="84%" align="left" valign="middle">{{$task->software}}</td>
  </tr>
  <tr>
    <th align="left" valign="middle" scope="row">Estimated time</th>
    <td align="left" valign="middle">{{$task->estimated_time_hours}}:{{$task->estimated_time_mins}}</td>
  </tr>
  <tr>
    <th align="left" valign="middle" scope="row">Project type</th>
    <td align="left" valign="middle">{{$task->project_type}}</td>
  </tr>
  <tr>
    <th align="left" valign="middle" scope="row">Language pair</th>
    <td align="left" valign="middle">{{$task->language_source}} -> {{$task->language_target}}</td>
  </tr>
  <tr>
    <th align="left" valign="middle" scope="row">Start time</th>
    <td align="left" valign="middle">{{ Carbon\Carbon::parse($task->start_time)->format('D d, M Y H:i a') }}</td>
  </tr>
  <tr>
    <th align="left" valign="middle" scope="row">Email subject</th>
    <td align="left" valign="middle">{{$task->email_subject}}</td>
  </tr>
  <tr>
    <th align="left" valign="middle" scope="row">Share path</th>
    <td align="left" valign="middle">{{$task->share_path}}</td>
  </tr>
</table>
