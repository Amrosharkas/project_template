<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'userController@index')->name('index');




Route::group(['middleware' => ['auth'], 'prefix' => 'admin/users', 'as' => 'admin.users.'], function () {
    Route::get('/{status}', 'UserController@index')->name('index');

    Route::post('init', 'UserController@init')->name('init');
    Route::get('{id}/edit', 'UserController@edit')->name('edit');
    Route::delete('{id}/delete', 'UserController@delete')->name('delete');
    Route::post('{id}/recommission', 'UserController@recommission')->name('recommission');
    Route::get('create', 'UserController@create')->name('create');
    Route::delete('delete_multiple', 'UserController@deleteMultiple')->name('delete_multiple');
    Route::post('store', 'UserController@store')->name('store');
    Route::patch('{id}/update', 'UserController@update')->name('update');
});



Route::group(['middleware' => ['auth'], 'prefix' => "admin/temp", 'as' => "admin.temp."], function () {
    Route::get('/', 'candidateController@index')->name('index');
    Route::delete('{id}/delete', 'candidateController@delete')->name('delete');
    Route::get('{id}/edit', 'candidateController@edit')->name('edit');
    Route::get('add', 'candidateController@add')->name('add');
    Route::post('save', 'candidateController@save')->name('save');

});

Route::auth();

