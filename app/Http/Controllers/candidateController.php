<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;

use Hash;
use Mail;



class candidateController extends Controller
{
    public function index() {
    	$candidates = User::get();
    	
        $data = [];
        $data['partialView'] = 'temp.list';
        $data['candidates'] = $candidates;
        $data['user_phase'] = "";
        $data['user_status'] = "";
        return view('temp.base', $data);
    }

    

   

    public function add(){
        $data = [];
        $data['partialView'] = 'temp.form';
        
        $data['user_phase'] = "";
        $data['user_status'] = "";
        return view('temp.base', $data);
    }

    

    public function save(Request $request){
        $data = $request->input();
        //Validations
        $check_email = User::where('email',$data['email'])->count();
        if($check_email !=0){
            $data = [];
            $data['status'] = 'error';
            
            $data['msg'] = "Email already exists";
            return response()->json(
                        $data
            );
        }
        
        $candidate = new User();
        $candidate->name = $data['name'];
        $candidate->email = $data['email'];
        $candidate->phone = $data['mobile'];
        $candidate->user_type = "Volunteer";
        $candidate->dec_password = $data['password'];
        $candidate->password = Hash::make($data['password']);
        if($candidate->save()){
            $data = [];
            $data['status'] = 'success';
            $data['page'] = '/admin/temp';
            $data['msg'] = "Saved Successfully";
            return response()->json(
                        $data
            );  
        }
        
        

        
        
    }
    
    

    public function delete($id){
        return 'success';
    }
}
