<?php

use Illuminate\Database\Seeder;
use App\User;
use App\quiz;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@nouracademy.com';
        $user->password = bcrypt('123456');
        $user->dec_password = bcrypt('123456');
        $user->user_type = 'Admin';
        $user->save();
        
    }
}
